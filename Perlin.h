#ifndef MINECRAFT_PERLIN_H
#define MINECRAFT_PERLIN_H

#include <vector>
#include <glm/glm.hpp>
#include <random>

class Perlin {
public:
    Perlin(unsigned int seed, unsigned int n) : seed(seed), n(n) {
        std::mt19937 gen(seed);
        std::uniform_real_distribution<> dis(-1.0, 1.0);

        gradients = std::vector<std::vector<std::vector<glm::vec3>>>(n+1);
        for (int i = 0; i < n; i++) {
            gradients[i] = std::vector<std::vector<glm::vec3>>(n+1);
            for (int j = 0; j < n; j++) {
                gradients[i][j] = std::vector<glm::vec3>(n+1);
                for (int k = 0; k < n; k++) {
                    gradients[i][j][k] = glm::normalize(glm::vec3(dis(gen), dis(gen), dis(gen)));
                }
            }
        }
    }

    unsigned int seed;
    unsigned int n;

    std::vector<std::vector<std::vector<glm::vec3>>> gradients {};

    double generate(double x, double y, double z);
};


#endif//MINECRAFT_PERLIN_H
