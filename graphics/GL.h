//
// Created by florian on 12/21/20.
//

#ifndef MINECRAFT_GL_H
#define MINECRAFT_GL_H

#include <GL/glew.h>
#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

class GL {
public:
    static GLuint loadProgram(std::vector<GLuint>& shaders) {
        GLuint program;

        program = glCreateProgram();
        for (auto s : shaders) {
            glAttachShader(program, s);
        }
        glLinkProgram(program);
        GLint status;
        glGetProgramiv(program, GL_LINK_STATUS, &status);
        if (status != GL_TRUE) {
            GLsizei len;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
            auto log = std::make_unique<GLchar *>(new GLchar[len + 1]);
            glGetProgramInfoLog(program, len, &len, *log);
            throw std::runtime_error(std::string("Linking failed: ") + *log);
        }

        return program;
    }

    static GLuint loadShader(const std::string& file, GLenum type) {
        std::ifstream f(file);
        std::stringstream source;
        source << f.rdbuf();

        std::string str = source.str();
        str.push_back('\0');

        const char *strData = str.data();

        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &strData, nullptr);
        glCompileShader(shader);
        GLint status;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE) {
            GLsizei len;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
            auto log = std::make_unique<GLchar *>(new GLchar[len + 1]);
            glGetShaderInfoLog(shader, len, &len, *log);
            throw std::runtime_error(std::string("Compilation failed: ") + *log);
        }

        return shader;
    }
};


#endif//MINECRAFT_GL_H
