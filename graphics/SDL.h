#include <array>

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>


template<class T>
class SDL {
public:
    SDL() {
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            throw std::runtime_error(std::string("Unable to initialize SDL: ") + SDL_GetError());
        }

        win = SDL_CreateWindow("Minecraft", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600,
                               SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        if (!win) {
            throw std::runtime_error(std::string("Unable to create window: ") + SDL_GetError());
        }

        glewExperimental = GL_TRUE;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        context = SDL_GL_CreateContext(win);
        if (!context) {
            throw std::runtime_error(std::string("Unable to create OpenGL context: ") + SDL_GetError());
        }

        std::array<GLint, 2> glVersion;
        glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
        glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
        std::cout << "Running OpenGL: " << glVersion[0] << '.' << glVersion[1] << std::endl;

        GLenum error = glewInit();
        if (error != GLEW_OK) {
            throw std::runtime_error(std::string("Unable to initialize GLEW: ") + reinterpret_cast<const char *>((glewGetErrorString(error))));
        }

        SDL_SetRelativeMouseMode(SDL_TRUE);
        mouseGrab = true;
        app.init();

        glViewport(0, 0, 800, 600);
        SDL_GL_SetSwapInterval(1);
    }

    void loop() {
        bool running = true;
        uint32_t fpsLast = SDL_GetTicks();
        uint32_t fpsCount = 0;

        SDL_Event event;
        while (running) {
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    running = false;
                } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
                    toggleMouseGrab();
                } else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT) {
                    SDL_SetRelativeMouseMode(SDL_TRUE);
                    mouseGrab = true;
                }

                if (mouseGrab || event.type != SDL_MOUSEMOTION) {
                    app.event(&event);
                }
            }

            app.render();
            SDL_GL_SwapWindow(win);
            fpsCount++;
            unsigned int ticks = SDL_GetTicks();
            if (fpsLast < ticks - 5000 && ticks > 5000) {
                fpsLast = SDL_GetTicks();
                std::cout << "FPS: " << fpsCount/5.0 << std::endl;
                fpsCount = 0;
            }
        }
    }

    ~SDL() {
        SDL_Quit();
    }

private:
    SDL_Window *win;
    SDL_GLContext context;

    bool mouseGrab;
    void toggleMouseGrab() {
        if (mouseGrab) {
            SDL_SetRelativeMouseMode(SDL_FALSE);
            mouseGrab = false;
        } else {
            SDL_SetRelativeMouseMode(SDL_TRUE);
            mouseGrab = true;
        }
    }

    T app;
};
