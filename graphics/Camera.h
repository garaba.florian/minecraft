#ifndef MINECRAFT_CAMERA_H
#define MINECRAFT_CAMERA_H

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// TODO: Many optimizations available (eg.: multiply known matrices compile time)
class Camera {
public:
    Camera() {
        recalculateViewMatrix();
    }

    void placeEye(glm::vec3 p) {
        eye = p;
        recalculateViewMatrix();
    }

    void setAspectRation(float ratio) {
        this->ratio = ratio;

        recalculateViewMatrix();
    }

    void translate(glm::vec3 p) {
        p *= 7;
        glm::mat3 m(1.0, 0, 0, 0, 0, 0, 0, 0, 1.0);
        glm::mat3 m2(0, 0, 0, 0, 1.0, 0, 0, 0, 0);
        glm::vec3 v = glm::cross(u, w);
        eye = eye - p[0]*glm::normalize(m*v) - p[1]*glm::normalize(m2*u) - p[2]*glm::normalize(m*w);

        recalculateViewMatrix();
    }

    void rotate(float y, float x) {
        glm::mat3 rotY(cos(y), 0, -sin(y),
                       0, 1, 0,
                       sin(y), 0, cos(y));

        glm::mat3 rotX(1, 0, 0,
                       0, cos(x), sin(x),
                       0, -sin(x), cos(x));

        w = rotY * rotX *  glm::vec3(0, 0, -1);
        u = rotY * rotX * glm::vec3(0, 1, 0);

        recalculateViewMatrix();
    }

    const glm::mat4 &getViewMatrix() const {
        return viewMatrix;
    }

    const glm::vec3 getEye() const {
        return eye;
    }

    const glm::vec3 getW() const {
        return -w;
    }

private:
    glm::vec3 eye{0, 0, 3};

    glm::vec3 w{0, 0, 1};
    glm::vec3 u{0, 1, 0};

    float ratio = 800.0f/600;

    glm::mat4 viewMatrix;

    void recalculateViewMatrix() {
        glm::mat4 camMx = glm::lookAt(eye, eye + w, u);
        viewMatrix = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 400.0f) * camMx;

        static int n = 0;
        n = (n+1) % 20;
        if (n == 0) {
            std::cout << "Camera location: " << eye[0] << " " << eye[1] << " " << eye[2] << std::endl;
        }
    }
};

#endif//MINECRAFT_CAMERA_H
