#include "Minecraft.h"

#include "Perlin.h"
#include "graphics/GL.h"
#include "graphics/stb_image.h"

#include <GL/glew.h>
#include <algorithm>
#include <stack>
#include <vector>

void Minecraft::init() {
    GLuint vertexArray;

    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);

    generateTerrain(20);

    Material m = Material::AIR;
    int i = Chunk::CHUNK_Y-1;
    while (m == Material::AIR) {
        m = chunks[0].blocks[1][i][1].m;
        i--;
    }
    camera.placeEye(glm::vec3(0,i+10,0));
    uploadChunks();

    int width, height, nrChannels;
    unsigned char *data = stbi_load("shaders/texture.png", &width, &height, &nrChannels, 0);
    if (data == nullptr) {
        throw std::runtime_error("Could not load texture");
    }

    glGenTextures(1, &textureAtlas);
    glBindTexture(GL_TEXTURE_2D, textureAtlas);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(data);

    bufferIds.reserve(1);
    glCreateBuffers(1, bufferIds.data());

    crosshair[0] = {-0.015, 0};
    crosshair[1] = {0.015, 0};
    crosshair[2] = {0, -0.015};
    crosshair[3] = {0, 0.015};

    glNamedBufferStorage(bufferIds[0], crosshair.size() * sizeof(crosshair[0]), crosshair.data(), 0);

    std::vector<GLuint> shaders(3);
    shaders.push_back(GL::loadShader("shaders/geometry.glsl", GL_GEOMETRY_SHADER));
    shaders.push_back(GL::loadShader("shaders/vertex.glsl", GL_VERTEX_SHADER));
    shaders.push_back(GL::loadShader("shaders/fragment.glsl", GL_FRAGMENT_SHADER));

    std::vector<GLuint> crosshairShaders(2);
    crosshairShaders.push_back(GL::loadShader("shaders/chVertex.glsl", GL_VERTEX_SHADER));
    crosshairShaders.push_back(GL::loadShader("shaders/chFragment.glsl", GL_FRAGMENT_SHADER));

    glProgram = GL::loadProgram(shaders);
    chProgram = GL::loadProgram(crosshairShaders);

    glUseProgram(glProgram);

    projectionUniform = glGetUniformLocation(glProgram, "projection");
    glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, &(camera.getViewMatrix())[0][0]);

    sunUniform = glGetUniformLocation(glProgram, "sun");
    auto sv = getSunVector();
    glUniform3f(sunUniform, sv[0], sv[1], sv[2]);

    cameraUniform = glGetUniformLocation(glProgram, "camera");
    glUniform3f(cameraUniform, camera.getEye()[0], camera.getEye()[1], camera.getEye()[2]);
    viewUniform = glGetUniformLocation(glProgram, "view");
    glUniform3f(viewUniform, camera.getW()[0], camera.getW()[1], camera.getW()[2]);

    chunkPosUniform = glGetUniformLocation(glProgram, "chunkPos");


    glUseProgram(chProgram);
    aspectUniform = glGetUniformLocation(chProgram, "aspect");
    glUniform1f(aspectUniform, 800.0/600);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(0.13, 0.13, 0.13, 1);
}

void Minecraft::render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(chProgram);
    glBindBuffer(GL_ARRAY_BUFFER, bufferIds[0]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glDrawArrays(GL_LINES, 0, 4);

    glUseProgram(glProgram);
    for (Chunk c : chunks) {
        glUniform3f(chunkPosUniform, c.pos[0]*Chunk::CHUNK_X, 0, c.pos[1]*Chunk::CHUNK_Z);

        glBindBuffer(GL_ARRAY_BUFFER, c.id);
        glVertexAttribIPointer(0, 1, GL_UNSIGNED_INT, 0, 0);
        glEnableVertexAttribArray(0);

        glDrawArrays(GL_POINTS, 0, c.count);
    }
}

void Minecraft::event(SDL_Event *event) {
    static GLfloat cameraRotY;
    static GLfloat cameraRotX;

    if (event->type == SDL_MOUSEMOTION) {
        cameraRotY -= event->motion.xrel / 500.0;
        cameraRotX -= event->motion.yrel / 500.0;

        camera.rotate(cameraRotY, cameraRotX);

        glUseProgram(glProgram);
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, &(camera.getViewMatrix())[0][0]);
        glUniform3f(viewUniform, camera.getW()[0], camera.getW()[1], camera.getW()[2]);

    } else if (event->type == SDL_KEYDOWN) {
        switch (event->key.keysym.sym) {
            case SDLK_w:
                camera.translate(glm::vec3(0, 0, -0.1));
                break;
            case SDLK_a:
                camera.translate(glm::vec3(-0.1, 0, 0));
                break;
            case SDLK_s:
                camera.translate(glm::vec3(0, 0, 0.1));
                break;
            case SDLK_d:
                camera.translate(glm::vec3(0.1, 0, 0));
                break;
            case SDLK_e:
                camera.translate(glm::vec3(0, 0.1, 0));
                break;
            case SDLK_r:
                camera.translate(glm::vec3(0, -0.1, 0));
                break;
            case SDLK_u:
                glUseProgram(glProgram);
                sunDegree += 0.05;
                {
                    auto sv = getSunVector();
                    glUniform3f(sunUniform, sv[0], sv[1], sv[2]);
                }
                break;
            case SDLK_i:
                glUseProgram(glProgram);
                sunDegree -= 0.05;
                {
                    auto sv = getSunVector();
                    glUniform3f(sunUniform, sv[0], sv[1], sv[2]);
                }
                break;
        }

        glUseProgram(glProgram);
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, &(camera.getViewMatrix())[0][0]);
        glUniform3f(cameraUniform, camera.getEye()[0], camera.getEye()[1], camera.getEye()[2]);
    } else if (event->type == SDL_WINDOWEVENT && event->window.event == SDL_WINDOWEVENT_RESIZED) {
        float ratio = (float) event->window.data1 / event->window.data2;
        glUseProgram(chProgram);
        glUniform1f(aspectUniform, ratio);
        camera.setAspectRation(ratio);
        glViewport(0, 0, (GLint) event->window.data1, (GLint) event->window.data2);
    }
}
void Minecraft::generateTerrain(int n) {

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(1, 20);

    const int seed1 = dis(gen);
    const int seed2 = dis(gen);
    const double caveDim = 3 * n;
    const double heightDim = 4 * n;
    const double biomDim = 3 * n;
    Perlin caveText(seed1, caveDim);
    Perlin heightText(seed2, heightDim);
    Perlin heightText2(seed2+3, heightDim-n);
    Perlin biomText(seed2+1, biomDim);

    for (int cx = 0; cx < n; cx++) {// generate n*n chunks
        for (int cy = 0; cy < n; cy++) {
            Chunk c{};
            c.pos = glm::vec2(cx, cy);
            for (int x = 0; x < Chunk::CHUNK_X; x++) {
                for (int z = 0; z < Chunk::CHUNK_Z; z++) {
                    double biom = biomText.generate((x+cx*Chunk::CHUNK_X) * (biomDim-1) / 18.0 / (n*Chunk::CHUNK_X),
                                                    0.36,
                                                    (z+cy*Chunk::CHUNK_Z) * (biomDim-1) / 18.0 / (n*Chunk::CHUNK_Z));
                    double height = heightText.generate((x+cx*Chunk::CHUNK_X) * (heightDim-1) / 10.5 / (n*Chunk::CHUNK_X),
                                                        0.6,
                                                        (z+cy*Chunk::CHUNK_Z) * (heightDim-1) / 10.5 / (n*Chunk::CHUNK_Z));
                    double height2 = heightText2.generate((x+cx*Chunk::CHUNK_X) * (heightDim-1) / 16.0 / (n*Chunk::CHUNK_X),
                                                        0.3,
                                                        (z+cy*Chunk::CHUNK_Z) * (heightDim-1) / 16.0 / (n*Chunk::CHUNK_Z));

                    height = ((height+height2*1.45)*(1-std::clamp(biom+0.24, 0.0, 0.5)*1.91)) * (Chunk::CHUNK_Y / 7.6) + (Chunk::CHUNK_Y / 4.0);

                    for (int y = 0; y < Chunk::CHUNK_Y; y++) {
                        if (y < height) {
                            double m = caveText.generate((x+cx*Chunk::CHUNK_X) * (caveDim-1) / 1.0 / (n*Chunk::CHUNK_X),
                                                         y * (caveDim-1) / 1.0 / (Chunk::CHUNK_Y),
                                    (z+cy*Chunk::CHUNK_Z) * (caveDim-1) / 1.0 / (n*Chunk::CHUNK_Z));


                            double hr = y/height;
                            if (m > 0.16 && hr < 0.83) {
                                c.blocks[x][y][z].m = Material::AIR;
                            } else {
                                if (hr > 0.96) {
                                    if (biom > 0.16) {
                                        c.blocks[x][y][z].m = Material::SAND;
                                    } else {
                                        c.blocks[x][y][z].m = Material::GRASS;
                                    }
                                } else if (hr > 0.88 || (hr > 0.82 && m > 0.1)) {
                                    c.blocks[x][y][z].m = Material::DIRT;
                                } else {
                                    if (m > 0.14 && m*2 > hr) {
                                        c.blocks[x][y][z].m = Material::COAL;
                                    } else {
                                        c.blocks[x][y][z].m = Material::STONE;
                                    }
                                }
                            }
                        } else {
                            c.blocks[x][y][z].m = Material::AIR;
                        }
                    }
                }
            }

            chunks.push_back(c);
        }
    }
}

unsigned int encodeBlock(unsigned int a, unsigned int b, unsigned int c, int d) {
    return (a) | (b << 4) | (c << 12) | (d << 20);
}

void Minecraft::uploadChunks() {
    int count = chunks.size();
    std::vector<GLuint> ids(count);
    glCreateBuffers(count, ids.data());


    int step = 0;
    for (int i = 250; i > 1; i--) {
        if (i < 50 && i > 37) {
            step++;
        }
        chunks[0].blocks[10+step][i][10].m = Material::AIR;
        chunks[0].blocks[10+step][i-1][10].m = Material::AIR;
        chunks[0].blocks[10+step][i-2][10].m = Material::AIR;
    }

    std::array<unsigned int, Chunk::CHUNK_X * Chunk::CHUNK_Y * Chunk::CHUNK_Z> data{};
    int dataIndex;
    for (int i = 0; i < count; i++) {
        dataIndex = 0;

        std::array<unsigned int, 3> startCoord = {0,Chunk::CHUNK_Y-1,0};
        chunks[i].blocks[0][Chunk::CHUNK_Y-1][0].visit = true;
        std::stack<std::array<unsigned int, 3>> stack;
        stack.push(startCoord);

        while (!stack.empty()) {
            auto coord = stack.top();
            stack.pop();

            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    for (int z = -1; z < 2; z++) {
                        if (x == 0 && y == 0 && z == 0) {
                            continue;
                        }
                        if (coord[0] + x < 0 || coord[0] + x >= Chunk::CHUNK_X || coord[1] + y < 0
                                || coord[1] + y >= Chunk::CHUNK_Y
                                || coord[2] + z < 0 || coord[2] + z >= Chunk::CHUNK_Z) {
                            continue;
                        }

                        Block &block = chunks[i].blocks[coord[0] + x][coord[1] + y][coord[2] + z];
                        if (!block.visit && block.m == Material::AIR) {
                            block.visit = true;
                            stack.push({coord[0]+x,coord[1]+y,coord[2]+z});
                        } else if (!block.visit) {
                            block.visit = true;
                            data[dataIndex] = encodeBlock(coord[0] + x,
                                               coord[1] + y,
                                               coord[2] + z,
                                                          (int) block.m);
                            dataIndex++;
                        }
                    }
                }
            }
        }

       /* for (int x = 0; x < Chunk::CHUNK_X; x++) {
            for (int y = 0; y < Chunk::CHUNK_Y; y++) {
                for (int z = 0; z < Chunk::CHUNK_Z; z++) {
                    data[x * Chunk::CHUNK_Y * Chunk::CHUNK_Z + y * Chunk::CHUNK_Z + z] =
                            {(float) x + chunks[i].pos[0]*Chunk::CHUNK_X,
                             (float) y,
                             (float) z + chunks[i].pos[1]*Chunk::CHUNK_Z,
                             (float) chunks[i].blocks[x][y][z].m};
                }
            }
        }*/

        chunks[i].id = ids[i];
        chunks[i].count = dataIndex;
        glNamedBufferStorage(ids[i], sizeof(data[0]) * dataIndex, data.data(), 0);
        std::cout << "Uploaded chunk with id " << ids[i] << std::endl;
    }
}
glm::vec3 Minecraft::getSunVector() {
    return glm::normalize(glm::vec3(sin(sunDegree), -cos(sunDegree), 0.3));
}
