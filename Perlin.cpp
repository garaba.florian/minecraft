#include "Perlin.h"

double lerp(double a, double b, double t) {
    double d = b-a;
    return (6*pow(t,5)-15*pow(t,4)+10*pow(t,3)) * d + a;
}

double Perlin::generate(double x, double y, double z) {
    int i = (int)x;
    int j = (int)y;
    int k = (int)z;

    double u = x-i;
    double v = y-j;
    double w = z-k;

    std::vector<float> r;
    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            for (int c = 0; c < 2; c++) {
                if (i+a==n || j+b==n || k+c==n) {
                    continue;
                }
                r.push_back(glm::dot(gradients[i+a][j+b][k+c], glm::vec3(i+a, j+b, k+c) - glm::vec3(x,y,z)));
            }
        }
    }

    double dw1 = lerp(r[0], r[1], w);
    double dw2 = lerp(r[2], r[3], w);
    double dv1 = lerp(dw1, dw2, v);

    double dw3 = lerp(r[4], r[5], w);
    double dw4 = lerp(r[6], r[7], w);
    double dv2 = lerp(dw3, dw4, v);

    return lerp(dv1, dv2, u);
}
