{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [ clang glm pkgconfig SDL2 glew glew.dev freeglut ];
}
