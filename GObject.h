#ifndef MINECRAFT_GOBJECT_H
#define MINECRAFT_GOBJECT_H

#include <GL/glew.h>
#include <array>
#include <glm/glm.hpp>

enum class Material {
    AIR=0, GRASS, DIRT, STONE, SAND, COAL
};

struct Block {
    Material m;
    bool visit = false;
};

class Chunk {
public:
    static const int CHUNK_X = 16;
    static const int CHUNK_Y = 256;
    static const int CHUNK_Z = 16;

    GLuint id;
    unsigned int count;
    glm::vec2 pos;
    std::array<std::array<std::array<Block, CHUNK_X>, CHUNK_Y>, CHUNK_Z> blocks;

};


#endif//MINECRAFT_GOBJECT_H
