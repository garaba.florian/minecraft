#ifndef MINECRAFT_MINECRAFT_H
#define MINECRAFT_MINECRAFT_H

#include "GObject.h"
#include "graphics/Camera.h"

#include <GL/glew.h>
#include <SDL2/SDL_events.h>
#include <iostream>
#include <vector>

class Minecraft {
public:
    void init();

    void event(SDL_Event *event);

    void render();

private:
    Camera camera;

    GLuint glProgram;
    GLuint chProgram;
    GLuint projectionUniform;
    GLuint cameraUniform;
    GLuint viewUniform;
    GLuint sunUniform;
    GLuint aspectUniform;
    GLuint chunkPosUniform;

    std::vector<GLuint> bufferIds;

    std::vector<Chunk> chunks;

    GLuint textureAtlas;

    double sunDegree = 0;

    void generateTerrain(int n);
    void uploadChunks();
    glm::vec3 getSunVector();

    std::array<std::array<GLfloat,2>, 4> crosshair;
};


#endif//MINECRAFT_MINECRAFT_H
