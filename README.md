A very basic Minecraft clone made for a final (in a bit of a rush, at the detriment of code quality at some places).

# What's implemented:
- terrain generation, with Perlin noise (both 2D and 3D)
- free flying mode
- basic shaders

# Build
The project uses SDL2 and glm, and CMake as a build tool.
On NixOS, one can just issue `nix-shell` to get all the required dependencies.

Afterwards run the following

```
mkdir build
cd build
cmake ..
make
```

# Keybindings

- WASD for moving
- E/R for down/up respectively
- U/I for changing the Sun's position
- ESC for releasing mouse
