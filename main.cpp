#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <algorithm>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "Minecraft.h"
#include "graphics/SDL.h"

int main() {
  SDL<Minecraft> sdl;
  sdl.loop();

  return 0;
}
