#version 460 core

layout(location = 0) in int vPosition;

out float material;

uniform vec3 chunkPos;

void main() {
  int x = vPosition & 0xf;
  int y = (vPosition >> 4) & 0xff;
  int z = (vPosition >> 12) & 0xf;
  gl_Position = vec4(x, y, z, 1) + vec4(chunkPos, 0);
  material = (vPosition >> 20) & 0xf;
}
