#version 460 core

in vec4 pos;
in vec3 norm;
in float mat;

in vec2 texCoord;

out vec4 fColor;

uniform sampler2D textureAtlas;

uniform vec3 sun;
float sunStrength = clamp(cos(atan(sun.x, -sun.y)), 0, 1)*0.9;

void main() {
  vec3 baseColor = texture(textureAtlas, texCoord).xyz;

  float ks = 0.77;
  float kd = 0.44;
  float ka = 0.8;

  float specTerm = dot(2*dot(-sun, norm)*norm+sun, vec3(0,0,1));
  float diff = ka*(0.7*sunStrength+0.3) + clamp(kd*dot(-sun, norm)*sunStrength, 0, 1) + ks*specTerm*specTerm*specTerm*1.2*sunStrength;

  if (gl_FrontFacing) {
    fColor = vec4(diff*baseColor, 1);

//    fColor = vec4(vec3(0.9,0.2,0.1)*smoothstep(0.8, 1.0, 1-pos.z/50.0),1);
  } else {
    fColor = vec4(0.9,0.9,0,1);
  }
}
