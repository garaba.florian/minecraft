#version 460 core

layout (points) in;
layout (triangle_strip,max_vertices=3*4) out;

in float material[];
out vec4 pos;
out vec3 norm;
out float mat;
out vec2 texCoord;

uniform mat4 projection;
uniform vec3 camera;
uniform vec3 view;

vec2 textures[] = {
                    // GRASS
                    vec2(3/16.0,0), vec2(4/16.0,0), vec2(3/16.0,1/16.0), vec2(4/16.0,1/16.0),
                    vec2(0,0), vec2(1/16.0,0), vec2(0,1/16.0), vec2(1/16.0,1/16.0),
                    vec2(2/16.0,0), vec2(3/16.0,0), vec2(2/16.0,1/16.0), vec2(3/16.0,1/16.0),

                    // DIRT
                    vec2(2/16.0, 0), vec2(3/16.0, 0), vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0),
                    vec2(2/16.0, 0), vec2(3/16.0, 0), vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0),
                    vec2(2/16.0, 0), vec2(3/16.0, 0), vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0),

                    // STONE
                    vec2(1/16.0, 0), vec2(2/16.0, 0), vec2(1/16.0, 1/16.0), vec2(2/16.0, 1/16.0),
                    vec2(1/16.0, 0), vec2(2/16.0, 0), vec2(1/16.0, 1/16.0), vec2(2/16.0, 1/16.0),
                    vec2(1/16.0, 0), vec2(2/16.0, 0), vec2(1/16.0, 1/16.0), vec2(2/16.0, 1/16.0),

                    // SAND
                    vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0), vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0),
                    vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0), vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0),
                    vec2(2/16.0, 1/16.0), vec2(3/16.0, 1/16.0), vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0),

                    // COAL
                    vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0), vec2(2/16.0, 3/16.0), vec2(3/16.0, 3/16.0),
                    vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0), vec2(2/16.0, 3/16.0), vec2(3/16.0, 3/16.0),
                    vec2(2/16.0, 2/16.0), vec2(3/16.0, 2/16.0), vec2(2/16.0, 3/16.0), vec2(3/16.0, 3/16.0)
};

void setTexture(int side, int n, bool reverse) { // side = 0, top = 1, bottom = 2
    int m = int(mat)-1;
    if (reverse) {
        texCoord = textures[m*4*3+(3-n)+side*4];
    } else {
        texCoord = textures[m*4*3+n+side*4];
    }
}

vec4 project(vec4 v) {
    return projection * v;
}

void addV(vec4 p, vec3 t) {
    gl_Position = project(p + vec4(t,0));
    pos = gl_Position;
    EmitVertex();
}

void main() {
    vec4 p = gl_in[0].gl_Position;

    mat = material[0];

    vec3 cam = camera - (p.xyz + vec3(0.5, 0.5, 0.5));

    if (mat == 0 || dot(normalize(view), normalize(cam)) < 0.44) {
        return;
    }

    // BACK
    norm = vec3(0,0,1);
    if (dot(cam, norm) >= 0) {
        setTexture(0, 0, true);
        addV(p, vec3(0,0,1));

        setTexture(0, 1, true);
        addV(p, vec3(1,0,1));

        setTexture(0, 2, true);
        addV(p, vec3(0,1,1));

        setTexture(0, 3, true);
        addV(p, vec3(1,1,1));
        EndPrimitive();
    }

    // TOP
    norm = vec3(0,1,0);
    if (dot(cam, norm) >= 0) {
        setTexture(1, 0, false);
        addV(p, vec3(0, 1, 1));

        setTexture(1, 1, false);
        addV(p, vec3(1, 1, 1));

        setTexture(1, 2, false);
        addV(p, vec3(0, 1, 0));

        setTexture(1, 3, false);
        addV(p, vec3(1, 1, 0));
        EndPrimitive();
    }

    // FRONT
    norm = vec3(0,0,-1);
    if (dot(cam, norm) >= 0) {
        setTexture(0, 0, false);
        addV(p, vec3(0, 1, 0));

        setTexture(0, 1, false);
        addV(p, vec3(1, 1, 0));

        setTexture(0, 2, false);
        addV(p, vec3(0, 0, 0));

        setTexture(0, 3, false);
        addV(p, vec3(1, 0, 0));
        EndPrimitive();
    }

    // DOWN
    norm = vec3(0,-1,0);
    if (dot(cam, norm) >= 0) {
        setTexture(2, 0, false);
        addV(p, vec3(0, 0, 0));

        setTexture(2, 1, false);
        addV(p, vec3(1, 0, 0));

        setTexture(2, 2, false);
        addV(p, vec3(0, 0, 1));

        setTexture(2, 3, false);
        addV(p, vec3(1, 0, 1));
        EndPrimitive();
    }

    // LEFT
    norm = vec3(-1,0,0);
    if (dot(cam, norm) >= 0) {
        setTexture(0, 0, false);
        addV(p, vec3(0, 1, 1));

        setTexture(0, 1, false);
        addV(p, vec3(0, 1, 0));

        setTexture(0, 2, false);
        addV(p, vec3(0, 0, 1));

        setTexture(0, 3, false);
        addV(p, vec3(0, 0, 0));
        EndPrimitive();
    }

    // RIGHT
    norm = vec3(1,0,0);
    if (dot(cam, norm) >= 0) {
        setTexture(0, 0, false);
        addV(p, vec3(1, 1, 0));

        setTexture(0, 1, false);
        addV(p, vec3(1, 1, 1));

        setTexture(0, 2, false);
        addV(p, vec3(1, 0, 0));

        setTexture(0, 3, false);
        addV(p, vec3(1, 0, 1));
        EndPrimitive();
    }
}
