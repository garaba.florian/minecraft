#version 460 core

layout(location = 0) in vec2 vPosition;

uniform float aspect;

void main() {
  gl_Position = vec4(vPosition.x/aspect, vPosition.y, 0.1, 1);
}
